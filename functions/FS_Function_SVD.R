#Singular Value Decomposition (SVD) is a form of matrix analysis that leads to a low-dimensional representation of 
#a high-dimensional matrix. It allows us to easily eliminate the less important parts of generated matrix representation so as 
#to produce an approximate representation

FS_Function_SVD<-function(Tablefeatures, TH=NULL){
  
  #Tabletest= it is a dataframe with numeric variables
  #TH=Number of components
  
  #Libraries
  library(svd)
    
    #Chech if TH is NULL, default value is 2
    if(is.null(TH)) {
      b=2
    } else {
      b=TH
    }
    
    #chek if there are missing data
    indexNA=which(is.na(Tablefeatures))
    if (length(indexNA)>0){
      stop("This is an error message, because your data contains NaNs")
    }
    
    
    #Because you can't divide by the standard deviation if it's infinity. 
    #To identify the zero-variance column
    index=which(apply(Tablefeatures, 2, var)==0)
    
    #to remove zero variance columns from the dataset
    Tablefeatures=Tablefeatures[ , apply(Tablefeatures, 2, var) != 0]
    
    
    #Running SVD
    #Transform dataset in a matrix
    TablefeaturesMatrix=data.matrix(Tablefeatures)
    #Scale the data before appling SVD
    
    svd_out <- svd(scale(TablefeaturesMatrix))
    
    # The returned value of svd_out is a list with components
    # 
    # d: a vector containing the singular values of x, of length min(n, p), sorted decreasingly.
    # 
    # u: a matrix whose columns contain the left singular vectors of x, present if nu > 0. Dimension c(n, nu).
    # 
    # v: a matrix whose columns contain the right singular vectors of x, present if nv > 0. Dimension c(p, nv).
    
    #Calculate variance
    variance.explained = prop.table(svd(scale(TablefeaturesMatrix))$d^2)
    
    #Plot Variance
    plot(svd_out$d^2/sum(svd_out$d^2), xlim = c(0, 15), type = "b", pch = 16, xlab = "principal components", 
         ylab = "variance explained")
    PLOTVARIANCE=recordPlot()
    
    #Plot of first and second componenets
    par(mfrow=c(1,2))
    plot(svd_out$v[,1], ylab="1st PC")
    plot(svd_out$v[,2], ylab="2nd PC")
    h=recordPlot()
    
    
    #DataFrame of desidered components
    SVDVAr<-scale(TablefeaturesMatrix) %*% svd_out$v[,1:TH]  #PC scores from SVD
    SVDVAr=as.data.frame(SVDVAr)
    
    return(list(PLOTVARIANCE=PLOTVARIANCE, SVDOutput=svd_out, PLOT_First_second_components=h, SVDVARIABLES=SVDVAr ))

}
    
