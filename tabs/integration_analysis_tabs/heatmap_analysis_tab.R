heatmap_analysis_tab <- tabPanel("Heatmap Analysis",
  tags$br(),

  sidebarLayout(

    # Input(s)
    sidebarPanel(id="sidebar",

      # Bootstrap column number
      width = 3,
      fluid = TRUE,
      
      wellPanel(
        h4(tags$b("Data Subsetting")),

        column(width=6, textInput(inputId = 'setSeed', label   = 'Seed', value   = sample(1:10000,1))),

        column(width=6, numericInput(inputId = 'selRows', label = 'Number of Rows',min=1,max=10,value = 10)),

        column(width=12 ,selectizeInput('selCols',
                      'Columns Subset',
                      choices  = NULL,
                      multiple = T)),

        disabled(actionButton("goButton", "Create HeatMap"))
      ),
      
      wellPanel(
        h4(tags$b("Row dendrogram")),

        selectizeInput("distFun_row", "Distance method", c(Euclidean  = "euclidean",
                                                                          Maximum   = 'maximum',
                                                                          Manhattan = 'manhattan',
                                                                          Canberra  = 'canberra',
                                                                          Binary    = 'binary',
                                                                          Minkowski = 'minkowski'), selected = 'euclidean'),
                                                            
        selectizeInput("hclustFun_row", "Clustering linkage", c(Complete  = "complete",
                                                                                Single    = "single",
                                                                                Average   = "average",
                                                                                Mcquitt   = "mcquitty",
                                                                                Median    = "median",
                                                                                Centroid  = "centroid",
                                                                                Ward.D    = "ward.D",
                                                                                Ward.D2   = "ward.D2"), selected = 'complete'),

        sliderInput("r", "Number of Clusters", min = 1, max = 15, value = 2)  
      ),

      wellPanel(
        h4(tags$b("Column dendrogram")),

        selectizeInput("distFun_col", "Distance method", c(Euclidean  = "euclidean",
                                                                          Maximum   = 'maximum',
                                                                          Manhattan = 'manhattan',
                                                                          Canberra  = 'canberra',
                                                                          Binary    = 'binary',
                                                                          Minkowski = 'minkowski'), selected = 'euclidean'),

        selectizeInput("hclustFun_col", "Clustering linkage", c(Complete  = "complete",
                                                                                Single    = "single",
                                                                                Average   = "average",
                                                                                Mcquitty  = "mcquitty",
                                                                                Median    = "median",
                                                                                Centroid  = "centroid",
                                                                                Ward.D    = "ward.D",
                                                                                Ward.D2   = "ward.D2"), selected = 'complete'),

         sliderInput("c", "Number of Clusters", min = 1, max = 15, value = 2)
      ),

      wellPanel(
        h4(tags$b("Additional Parameters")),

        fluidRow(
          column(4,checkboxInput('showColor','Color', value = TRUE)),
          column(4,checkboxInput('showMargin','Layout')),
          column(4,checkboxInput('showDendo','Dendrogram'))
        ),


        conditionalPanel('input.showColor==1',
                          br(),
                          br(),
                          h5('Color Manipulation'),
                          hr(),
                          uiOutput('colUI'),
                          sliderInput("ncol", "Set Number of Colors", min = 1, max = 256, value = 256)
                          # checkboxInput('colRngAuto','Auto Color Range',value = T),
                          # conditionalPanel('!input.colRngAuto',
                          #                  uiOutput('colRng'))
        ),
        
        conditionalPanel('input.showMargin==1',
                          h5('Widget Layout'),
                          hr(),
                          column(4,textInput('main','Title','')),
                          column(4,textInput('xlab','X Title','')),
                          column(4,textInput('ylab','Y Title','')),
                          sliderInput('row_text_angle','Row Text Angle',value = 0,min=0,max=180),
                          sliderInput('column_text_angle','Column Text Angle',value = 45,min=0,max=180),
                          sliderInput("l", "Set Margin Width", min = 0, max = 200, value = 130),
                          sliderInput("b", "Set Margin Height", min = 0, max = 200, value = 40)
        ),

        conditionalPanel('input.showDendo==1',
                          h5('Dendrogram Manipulation'),
                          hr(),
                          selectInput('dendrogram','Dendrogram Type',choices = c("both", "row", "column", "none"),selected = 'both'),
                          selectizeInput("seriation", "Seriation", c(OLO="OLO",GW="GW",Mean="mean",None="none"),selected = 'OLO'),
                          sliderInput('branches_lwd','Dendrogram Branch Width',value = 0.6,min=0,max=5,step = 0.1)
        )
      )        
      # hr(),h3('Data Preprocessing'),
      # tags$br(),

      # selectizeInput('transpose',
      #                'Transpose',
      #                choices   = c('No'=FALSE,'Yes'=TRUE),
      #                selected  = FALSE),

      # selectizeInput("transform_fun", "Transform", c(Identity     =".",
      #                                                Sqrt         ='sqrt',
      #                                                log          ='log',
      #                                                Scale        ='scale',
      #                                                Normalize    ='normalize',
      #                                                Percentize   ='percentize',
      #                                                "Missing values"='is.na10', 
      #                                                Correlation  ='cor'),
      #                                                selected     = '.')
    ),

    # Output(s)
    mainPanel(
      width = 9,
      fluidRow(
        column(2,br()),
        column(10,plotlyOutput("heatout", height = "1100px", width = "1100px"))                   
      )
    )
  )
)






