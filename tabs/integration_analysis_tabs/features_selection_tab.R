features_selection_tab <- tabPanel("Features Selection", value = "fselection_data_panel",
                                   tags$br(),
                                   
                                   sidebarLayout(

                                     # Input(s)
                                     sidebarPanel(id = "sidebar5",
                                                  width = 3,
                                                  wellPanel(
                                                    
                                                    selectInput("fs_selected_experiments", "Select Experiment:", 
                                                                choices   = NULL,                      
                                                                selected  = NULL,
                                                                selectize = TRUE,
                                                                multiple  = FALSE),
                                                    
                                                    # Select normalization method
                                                    selectInput("fselectionFun", "Features Selection method", c(PCA = "pca",
                                                                                                                SVD = "svd"), selected = 'pca'
                                                    ),
                                                    
                                                    conditionalPanel(condition = "input.fselectionFun == 'pca'",
                                                                     numericInput("pca_type_choices", "Threshold Eigenvalue:", 1, min = 1, max = 1000)
                                                    ),
                                                    
                                                    conditionalPanel(condition = "input.fselectionFun == 'svd'",
                                                                     numericInput("svd_type_choices", "Number Of Components:", 1, min = 1, max = 1000)
                                                    ),
                                                    
                                                    actionButton("run_fs", "Run Features Selection", icon("chart-line"), style="color: #fff; background-color: #337ab7; border-color: #2e6da4"),

                                                    br(),
                                                    br()
                                                    
                                                    
                                                    # checkboxGroupInput("normalized_checkGroup",
                                                    #                    label = helpText("List Of Available Experiments:"),
                                                    #                    choices = NULL, FALSE),
                                                  ),
                                     ),
                                     
                                     # Output(s)
                                     mainPanel(width = 9,
                                               conditionalPanel(condition = "input.fselectionFun == 'pca'",
                                                 DT::dataTableOutput("pcs")
                                               ),
                                               
                                               conditionalPanel(condition = "input.fselectionFun == 'svd'",
                                                  DT::dataTableOutput("svdvariables")
                                               ),
                                               
                                               fluidRow(
                                                 column(6,
                                                        conditionalPanel(condition = "input.fselectionFun == 'pca'",
                                                                         plotOutput("graph_of_var")
                                                        ),
                                                        conditionalPanel(condition = "input.fselectionFun == 'svd'",
                                                                         imageOutput("plotvariance")
                                                        )
                                                 ),
                                                 column(6,
                                                        conditionalPanel(condition = "input.fselectionFun == 'pca'",
                                                          plotOutput("plot_var_contribution")
                                                        )
                                                 )
                                               ),
                                               fluidPage(
                                                 column(12,
                                                        conditionalPanel(condition = "input.fselectionFun == 'pca'",
                                                          plotOutput("first10")
                                                        )
                                                 )
                                               )
                                               #textOutput("test"),
                                               #withSpinner(plotOutput("PCAs"))
                                     )
                                   )
)
